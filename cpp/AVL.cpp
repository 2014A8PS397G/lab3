#include<bits/stdc++.h>

using namespace std;

struct Node
{
    int key;
    struct Node *left;
    struct Node *right;
    int height;
};

int K,D,x;
vector<int> insertlist, sequence;

int height(struct Node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

struct Node* newNode(int key)
{
    struct Node* node = (struct Node*)
                        malloc(sizeof(struct Node));
    node->key   = key;
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;  
    return(node);
}

int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

struct Node *rightRotate(struct Node *y)
{
  //rightrotate about node y and balance the two affected nodes in a bottom up fashion
    struct Node *x = y->left;
    struct Node *T2 = x->right;
    x->right = y;
    y->left = T2;
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
    return x;
}

struct Node *leftRotate(struct Node *x)
{
  //leftrotate about node x and balance the two affected nodes in a bottom up fashion
    struct Node *y = x->right;
    struct Node *T2 = y->left;
    y->left = x;
    x->right = T2;
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
    return y;   
}

struct Node* insert(struct Node* node, int key)
{
    if (node == NULL)//Tree is empty
        return(newNode(key));
 
    if (key < node->key)
        node->left  = insert(node->left, key);
    
    else if (key > node->key)
        node->right = insert(node->right, key);
        
    else
        return node;
        
    node->height = 1 + max(height(node->left),
                           height(node->right));
    
    int balance = getBalance(node);
    
    if (balance > 1 && key < node->left->key)
        return rightRotate(node);
    
    if (balance < -1 && key > node->right->key)
        return leftRotate(node);
    
    if (balance > 1 && key > node->left->key)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
    
    if (balance < -1 && key < node->right->key)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
    
    return node;
}

Node *lca(Node* root, int v1, int v2)
{   
    if(min(v1, v2) < root->key && max(v1, v2) < root->key)
        root = lca(root->left, v1, v2);
    else if(min(v1, v2) > root->key && max(v1, v2) > root->key)
        root = lca(root->right, v1, v2);
        
    return root;
}

int yourwork(Node* &root,vector<int> insertlist,vector<int> sequence){
   //   TODO
   // Insert all elements of insertlist into the tree;
     // Find max and min element of sequence
     // find lca for min and max element
     // return key of lca node
    
    for(int i = 0; i<K; i++)  
        root = insert(root, insertlist[i]);
    
    int max = sequence[0], min = sequence[0];
    for(int j = 0; j<D; j++){
        if(sequence[j] > max)
            max = sequence[j];
        if(sequence[j] < min)
            min = sequence[j];
    }  
    
    Node* least = lca(root, min, max);
    
    return least->key;
}
s


int main(){
   cin>>K;
   for(int i=0;i<K;i++){
      cin>>x;
      insertlist.push_back(x);
   }
   Node *root = newNode(0);
   cin>>D;
   for(int i=0;i<D;i++){
      cin>>x;
      sequence.push_back(x);
   }
   int ans = yourwork(root,insertlist,sequence);
   cout << ans << endl;
}

